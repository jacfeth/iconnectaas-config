<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:lookup="clientLookup.xml"
    extension-element-prefixes="lookup" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs" version="1.0">
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>

    <!-- Municipio -->
    <xsl:key name="municipio" match="lookup/municipalyLookup/string" use="@id"/>
    <xsl:variable name="munic" select='document("municipioLookup.xml")/lookup/municipalyLookup'/>

    
    <!-- Municipio invertido-->
    <xsl:key name="municipioIn" match="lookup/municipalyLookupIn/string" use="@id"/>
    <xsl:variable name="municIn" select='document("municipioLookup.xml")/lookup/municipalyLookupIn'/>
    
    <!-- Estado -->
    <xsl:key name="state" match="lookup/stateLookup/string" use="@id"/>
    <xsl:variable name="stat" select='document("stateLookup.xml")/lookup/stateLookup'/>
    <!-- Estado Invertido-->
    <xsl:key name="stateIn" match="lookup/stateLookupIn/string" use="@id"/>
    <xsl:variable name="statIn" select='document("stateLookup.xml")/lookup/stateLookupIn'/>
    
    <!-- Forma de Pago -->
    <xsl:key name="formaPago" match="formaPagoLookup/string" use="@id"/>
    <xsl:variable name="forma" select='document("formaDePagoLookup.xml")/formaPagoLookup'/>

    <!-- Metodo de Pago -->
    <xsl:key name="metodo" match="lookup/metodolookup/string" use="@id"/>
    <xsl:variable name="metodoPago" select='document("metodoPagoSatLookup.xml")/lookup/metodolookup'/>

    <!-- otra forma metodo -->
    <xsl:key name="metodoA" match="loopkup/metodoInlookup/string" use="@id"/>
    <xsl:variable name="metodoPago2"
        select='document("metodoPagoSatLookup.xml")/lookup/metodoInlookup'/>

    <!-- tipocliente -->
    <xsl:key name="tipc" match="tipoClientelookup/string" use="@id"/>
    <xsl:variable name="tipoCli" select='document("tipoClienteLookup.xml")/tipoClientelookup'/>

    <!-- CFDI -->
    <xsl:key name="usoCFDI" match="CFDILookup/string" use="@id"/>
    <xsl:variable name="usoCFD" select='document("usoCFDILookupTable.xml")/CFDILookup'/>



    <xsl:template match="/">
        <customer>
            <xsl:element name="recordControl">
                <xsl:element name="creationDate">
                    <xsl:value-of select="//customer/recordControl/creationDate"/>
                </xsl:element>
                <xsl:element name="modificationDate">
                    <xsl:value-of select="//customer/recordControl/modificationDate"/>
                </xsl:element>
                <xsl:element name="masterCreationDate">
                    <xsl:value-of select="//customer/recordControl/masterCreationDate"/>
                </xsl:element>
                <xsl:element name="masterModificationDate">
                    <xsl:value-of select="//customer/recordControl/masterModificationDate"/>
                </xsl:element>
            </xsl:element>
            <xsl:element name="id">
                <xsl:attribute name="appCode">
                    <xsl:value-of select="//customer/id/appCode"/>
                </xsl:attribute>
                <xsl:attribute name="type">
                    <xsl:value-of select="//customer/id/type"/>
                </xsl:attribute>

                <xsl:value-of select="//customer/id"/>

            </xsl:element>

            <xsl:element name="lastName">
                <xsl:value-of select="//lastName"/>
            </xsl:element>
            <xsl:element name="firstName">
                <xsl:value-of select="//firstName"/>
            </xsl:element>
            <xsl:element name="businessName">
                <xsl:value-of select="//businessName"/>
            </xsl:element>
            <xsl:element name="fiscalName">
                <xsl:value-of select="//fiscalName"/>
            </xsl:element>
            <xsl:element name="clasificationID">Externo</xsl:element>
            <xsl:element name="rfc">
                <xsl:value-of select="//rfc"/>
            </xsl:element>
            <xsl:element name="description">
                <xsl:value-of select="//description"/>
            </xsl:element>
            <xsl:element name="type">
                <xsl:if test="//type = 'ZCA_CUSTOMER'">
                    <xsl:attribute name="id">
                        <xsl:value-of select="1"/>
                    </xsl:attribute>Cliente</xsl:if>
                <xsl:if test="//type = 'ZCA_PROSPECT'">
                    <xsl:attribute name="id">
                        <xsl:value-of select="2"/>
                    </xsl:attribute> Cliente potencial</xsl:if>
            </xsl:element>
            <xsl:element name="siteURL">
                <xsl:value-of select="//siteURL"/>
            </xsl:element>
            <xsl:element name="salesAgent">
                <xsl:element name="id">
                    <xsl:value-of select="//salesAgent/id"/>
                </xsl:element>
                <xsl:element name="name">
                    <xsl:value-of select="//salesAgent/name"/>
                </xsl:element>
            </xsl:element>

            <xsl:element name="status">
                <xsl:element name="id">
                    <xsl:value-of select="//status/id"/>
                </xsl:element>
                <xsl:element name="name">
                    <xsl:value-of select="//status/name"/>
                </xsl:element>
            </xsl:element>
            <xsl:element name="thirdCustomerType">
                <xsl:value-of select="//thirdCustomerType"/>
            </xsl:element>
            <xsl:element name="transactionCode">
                <xsl:value-of select="//transactionCode"/>
            </xsl:element>
            <xsl:element name="comments">
                <xsl:for-each select="//comments">
                    <xsl:element name="id">
                        <xsl:value-of select="//comments/id"/>
                    </xsl:element>
                    <xsl:element name="text">
                        <xsl:value-of select="//comments/text"/>
                    </xsl:element>
                </xsl:for-each>
            </xsl:element>
            <xsl:element name="contacts">
                <xsl:for-each select="/customer/contacts/contact">
                    <xsl:variable name="pos" select="position()"/>
                    <xsl:element name="contact">
                        <xsl:element name="codeMethod">
                            <xsl:value-of select="//codeMethod"/>
                        </xsl:element>
                        <xsl:element name="comunicationMethod">
                            <xsl:value-of select="//comunicationMethod"/>
                        </xsl:element>
                        <xsl:element name="fax">
                            <xsl:value-of select="//fax"/>
                        </xsl:element>
                        <xsl:element name="firstContact">
                            <xsl:value-of select="//firstContact"/>
                        </xsl:element>
                        <xsl:element name="id">
                            <xsl:attribute name="appCode">
                                <xsl:value-of select="//id/appCode"/>
                            </xsl:attribute>
                            <xsl:attribute name="type">
                                <xsl:value-of select="//id/type"/>
                            </xsl:attribute>
                            <xsl:value-of select="//id/value"/>
                        </xsl:element>
                        <xsl:element name="isNotification">
                            <xsl:value-of select="//isNotification"/>
                        </xsl:element>
                        <xsl:element name="language">
                            <xsl:element name="id">
                                <xsl:value-of select="//language/id"/>
                            </xsl:element>
                            <xsl:element name="name">
                                <xsl:value-of select="//language/name"/>
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="lastname">
                            <xsl:value-of select="//lastname"/>
                        </xsl:element>
                        <xsl:element name="mail">
                            <xsl:value-of select="//mail"/>
                        </xsl:element>
                        <xsl:element name="name">
                            <xsl:value-of select="//customer/contacts[1]/contact[$pos]/name[1]"/>
                        </xsl:element>
                        <xsl:element name="officePhone">
                            <xsl:value-of select="//officePhone"/>
                        </xsl:element>
                        <xsl:element name="phone">
                            <xsl:value-of select="//phone"/>
                        </xsl:element>
                        <xsl:element name="status">

                            <xsl:attribute name="id">
                                <xsl:value-of select="//status/id"/>
                            </xsl:attribute>
                            <xsl:attribute name="appCode">
                                <xsl:value-of select="//status/appCode"/>
                            </xsl:attribute>
                            <xsl:element name="id">
                                <xsl:value-of select="//status/id"/>
                            </xsl:element>
                            <xsl:element name="type">
                                <xsl:value-of select="//status/type"/>
                            </xsl:element>
                            <xsl:element name="name">
                                <xsl:value-of select="//status/name"/>
                            </xsl:element>
                            <xsl:element name="value">
                                <xsl:value-of select="//status/value"/>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:for-each>
            </xsl:element>
            <xsl:element name="locations">
                <xsl:for-each select="//locations/location">
                    <xsl:element name="location">
                        <xsl:element name="address">
                            <xsl:element name="allAddress">
                                <xsl:value-of select="//address/allAddress"/>
                            </xsl:element>
                            <xsl:element name="city">
                                <xsl:element name="id">
                                    <xsl:value-of select="//address/city/id"/>
                                </xsl:element>
                                <xsl:element name="name">
                                    <xsl:value-of select="//address/city/name"/>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="country">
                                <xsl:element name="id">
                                    <xsl:value-of select="//address/country/id"/>
                                </xsl:element>
                                <xsl:element name="name">
                                    <xsl:value-of select="//address/country/name"/>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="entity">
                                <xsl:value-of select="//address/entity"/>
                            </xsl:element>
                            <xsl:element name="externalNumber">
                                <xsl:value-of select="//address/exteriorNumber"/>
                            </xsl:element>
                            <xsl:element name="id">
                                <xsl:value-of select="//address/id"/>
                            </xsl:element>
                            <xsl:element name="interiorNumber">
                                <xsl:value-of select="//address/interiorNumber"/>
                            </xsl:element>
                            <xsl:element name="latitude">
                                <xsl:value-of select="//address/latitude"/>
                            </xsl:element>
                            <xsl:element name="length">
                                <xsl:value-of select="//address/length"/>
                            </xsl:element>
                            <xsl:element name="locality">
                                <xsl:element name="code">
                                    <xsl:value-of select="//address/locality/code"/>
                                </xsl:element>
                                <xsl:element name="id">
                                    <xsl:value-of select="//address/locality/id"/>
                                </xsl:element>
                                <xsl:element name="name">
                                    <xsl:value-of select="//address/locality/name"/>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="municipality">
                                <xsl:element name="code">
                                    <xsl:value-of select="//address/municipality/code"/>
                                </xsl:element>
                                <xsl:element name="id">
                                    <xsl:value-of select="//address/municipality/id"/>
                                </xsl:element>

                                <xsl:element name="name">
                                    <xsl:variable name="algo" select="//address/municipality/id"/>
                                    <xsl:for-each select="$municIn">
                                        <xsl:value-of select='key("municipio", $algo)'/>
                                    </xsl:for-each>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="neighborhood">
                                <xsl:value-of select="//address/neighborhood"/>
                            </xsl:element>
                            <xsl:element name="population">
                                <xsl:element name="code">
                                    <xsl:value-of select="//address/population/code"/>
                                </xsl:element>
                                <xsl:element name="id">
                                    <xsl:value-of select="//address/population/id"/>
                                </xsl:element>
                                <xsl:element name="name">
                                    <xsl:value-of select="//address/population/name"/>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="postalCode">
                                <xsl:value-of select="//address/postalCode"/>
                            </xsl:element>
                            <xsl:element name="state">
                                <xsl:element name="code">
                                    <xsl:value-of select="//address/state/code"/>
                                </xsl:element>
                                <xsl:element name="id">
                                    <xsl:value-of select="//address/state/id"/>
                                </xsl:element>
                                <xsl:element name="name">
                                    <xsl:variable name="algo" select="//address/state/id"/>
                                    <xsl:for-each select="$stat">
                                        <xsl:value-of select='key("state", $algo)'/>
                                    </xsl:for-each>
                                   
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="street">
                                <xsl:value-of select="//address/street"/>
                            </xsl:element>
                            <xsl:element name="timeZone">
                                <xsl:attribute name="id">
                                    <xsl:value-of select="//address/timeZone/id"/>
                                </xsl:attribute>
                                <xsl:element name="value">
                                    <xsl:value-of select="//address/timeZone/value"/>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="contacts">
                            <xsl:for-each select="//contacts/contact">
                                <xsl:variable name="pos" select="position()"/>
                                <xsl:element name="contact">
                                    <xsl:element name="codeMethod">
                                        <xsl:value-of select="//codeMethod"/>
                                    </xsl:element>
                                    <xsl:element name="comunicationMethod">
                                        <xsl:value-of select="//comunicationMethod"/>
                                    </xsl:element>
                                    <xsl:element name="fax">
                                        <xsl:value-of select="//fax"/>
                                    </xsl:element>
                                    <xsl:element name="firtsContact">
                                        <xsl:value-of select="//firstContact"/>
                                    </xsl:element>
                                    <xsl:element name="id">
                                        <xsl:attribute name="appCode">
                                            <xsl:value-of select="//id/appCode"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="type">
                                            <xsl:value-of select="//id/type"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="//id/value"/>
                                    </xsl:element>
                                    <xsl:element name="isNotification">
                                        <xsl:value-of select="//isNotification"/>
                                    </xsl:element>
                                    <xsl:element name="language">
                                        <xsl:element name="id">
                                            <xsl:value-of select="//language/id"/>
                                        </xsl:element>
                                        <xsl:element name="name">
                                            <xsl:value-of select="//languaje/names"/>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="lastName">
                                        <xsl:value-of select="//lastname"/>
                                    </xsl:element>
                                    <xsl:element name="mail">
                                        <xsl:value-of select="//mail"/>
                                    </xsl:element>
                                    <xsl:element name="name">
                                        <xsl:value-of select="//contact[$pos]/name[1]"/>
                                    </xsl:element>
                                    <xsl:element name="officePhone">
                                        <xsl:value-of select="//officePhone"/>
                                    </xsl:element>
                                    <xsl:element name="phone">
                                        <xsl:value-of select="//phone"/>
                                    </xsl:element>
                                    <xsl:element name="status">
                                        <xsl:attribute name="appCode">
                                            <xsl:value-of select="//status/appCode"/>
                                        </xsl:attribute>
                                        <xsl:element name="id">
                                            <xsl:value-of select="//status/id"/>
                                        </xsl:element>
                                        <xsl:element name="name">
                                            <xsl:value-of select="//status/name"/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:for-each>
                        </xsl:element>
                        <xsl:element name="descripcition">
                            <xsl:value-of select="//description"/>
                        </xsl:element>
                        <xsl:element name="id">
                            <xsl:attribute name="appCode">
                                <xsl:value-of select="//id/appCode"/>
                            </xsl:attribute>
                            <xsl:attribute name="type">
                                <xsl:value-of select="//id/type"/>
                            </xsl:attribute>
                            <xsl:attribute name="typeName">
                                <xsl:value-of select="//id/typeName"/>
                            </xsl:attribute>

                            <xsl:value-of select="//id/value"/>

                        </xsl:element>
                        <xsl:element name="isTemplate">
                            <xsl:value-of select="//isTemplate"/>
                        </xsl:element>
                        <xsl:element name="isTemporary">
                            <xsl:value-of select="//isTemporary"/>
                        </xsl:element>
                        <xsl:element name="rol">
                            <xsl:element name="description">
                                <xsl:value-of select="//rol/description"/>
                            </xsl:element>
                            <xsl:element name="id">
                                <xsl:value-of select="//rol/id"/>
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="status">
                            <xsl:element name="id">
                                <xsl:value-of select="//status/id"/>
                            </xsl:element>
                            <xsl:element name="name">
                                <xsl:value-of select="//status/name"/>
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="transactionCode">
                            <xsl:value-of select="//transactionCode"/>
                        </xsl:element>
                    </xsl:element>
                </xsl:for-each>
            </xsl:element>
            <xsl:element name="paymentConfig">
                <xsl:element name="cfdiUse">
                    <xsl:element name="id">
                        <xsl:value-of select="//paymentConfig/cfdiUse/name"/>

                    </xsl:element>
                    <xsl:element name="name">
                        <xsl:variable name="algo" select="//paymentConfig/cfdiUse/name"/>

                        <xsl:for-each select="$usoCFD">
                            <xsl:value-of select='key("usoCFDI", $algo)'/>
                        </xsl:for-each>
                        <!-- <xsl:value-of select="//paymentConfig/cfdiUse/name"/> -->
                    </xsl:element>
                </xsl:element>
                <xsl:element name="paymentDeadLine">
                    <xsl:element name="id">
                        <xsl:value-of select="//paymentConfig/paymentDeadline/id"/>
                    </xsl:element>
                    <xsl:element name="name">
                        <xsl:value-of select="//paymentConfig/paymentDeadline/name"/>
                    </xsl:element>
                </xsl:element>
                <xsl:element name="paymentMethod">
                    <xsl:element name="id">
                        <xsl:value-of select="//paymentConfig/paymentMethod/id"/>
                    </xsl:element>
                    <xsl:element name="name">
                        <xsl:variable name="algo" select="//paymentConfig/paymentMethod/id"/>
                        <xsl:for-each select="$metodoPago">
                            <xsl:value-of select='key("metodo", $algo)'/>
                        </xsl:for-each>
                        <!-- <xsl:value-of select="//paymentConfig/paymentMethod/name"/>-->
                    </xsl:element>

                </xsl:element>
                <xsl:element name="paymentWay">
                    <xsl:element name="id">
                        <xsl:value-of select="//paymentConfig/paymentWay/name"/>
                    </xsl:element>
                    <xsl:element name="name">

                        <xsl:variable name="algo" select="//paymentConfig/paymentWay/name"/>
                        <xsl:for-each select="$forma">
                            <xsl:value-of select='key("formaPago", $algo)'/>
                        </xsl:for-each>


                        <!-- <xsl:value-of select="//paymentConfig/paymentWay/name"/>-->
                    </xsl:element>
                </xsl:element>
            </xsl:element>
        </customer>
    </xsl:template>
</xsl:stylesheet>
