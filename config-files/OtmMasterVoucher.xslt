<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output indent="yes"/>
    <xsl:template match="/">
        <message>
            <voucherDriver>
                <xsl:choose>
                    <xsl:when
                        test="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:PaymentHeader/*:ConsolidationType = 'STANDARD'">
                        <id>
                            <xsl:attribute name="appCode">
                                <xsl:value-of
                                    select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:VoucherGid/*:Gid/*:DomainName"
                                />
                            </xsl:attribute>
                            <xsl:value-of
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:VoucherGid/*:Gid/*:Xid"
                            />
                        </id>
                        <creationDate/>
                        <approvedDate>

                            <xsl:variable name="date"
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:ApprovedDate/*:GLogDate"/>
                            <xsl:if test="$date">
                                <xsl:variable name="year" select="substring($date, 0, 5)"/>
                                <xsl:variable name="mes" select="substring($date, 5, 2)"/>
                                <xsl:variable name="dia" select="substring($date, 7, 2)"/>
                                <xsl:variable name="hora" select="substring($date, 9, 2)"/>
                                <xsl:variable name="minuto" select="substring($date, 11, 2)"/>
                                <xsl:variable name="seg" select="'00'"/>


                                <xsl:variable name="newDate"
                                    select="concat($year, '-', $mes, '-', $dia)"/>

                                <xsl:variable name="tiemp"
                                    select="concat($hora, ':', $minuto, ':', $seg)"/>

                                <xsl:variable name="timeStamp"
                                    select="dateTime(xs:date($newDate), xs:time($tiemp))"/>
                                <xsl:variable name="fecha2" as="xs:string"
                                    select="format-dateTime($timeStamp, '[D01]/[M01]/[Y0001] [H01]:[m01]:[s01]')"/>
                                <xsl:value-of select="$fecha2"/>
                            </xsl:if>
                        </approvedDate>
                        <approvedBy>
                            <xsl:value-of
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:ApprovedBy"
                            />
                        </approvedBy>
                        <consolidationType>
                            <xsl:value-of select="//*:PaymentHeader/*:ConsolidationType"/>
                        </consolidationType>
                        <capturedBy>
                            <xsl:value-of
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:ApprovedBy"
                            />
                        </capturedBy>
                        <travel>
                            <id type="" appCode="">
                                <xsl:attribute name="appCode">
                                    <xsl:value-of
                                        select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Shipment/*:ShipmentHeader/*:ShipmentGid/*:Gid/*:DomainName"
                                    />
                                </xsl:attribute>
                                <xsl:value-of
                                    select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Shipment/*:ShipmentHeader/*:ShipmentGid/*:Gid/*:Xid"
                                />
                            </id>
                            <startDate/>
                            <endDate>
                                <xsl:variable name="enDate"
                                    select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Shipment/*:ShipmentHeader/*:EndDt/*:GLogDate"/>
                                <xsl:if test="$enDate">
                                    <xsl:variable name="year" select="substring($enDate, 0, 5)"/>
                                    <xsl:variable name="mes" select="substring($enDate, 5, 2)"/>
                                    <xsl:variable name="dia" select="substring($enDate, 7, 2)"/>
                                    <xsl:variable name="hora" select="substring($enDate, 9, 2)"/>
                                    <xsl:variable name="minuto" select="substring($enDate, 11, 2)"/>
                                    <xsl:variable name="seg" select="'00'"/>


                                    <xsl:variable name="newDate"
                                        select="concat($year, '-', $mes, '-', $dia)"/>

                                    <xsl:variable name="tiemp"
                                        select="concat($hora, ':', $minuto, ':', $seg)"/>

                                    <xsl:variable name="timeStamp"
                                        select="dateTime(xs:date($newDate), xs:time($tiemp))"/>
                                    <xsl:variable name="fecha2" as="xs:string"
                                        select="format-dateTime($timeStamp, '[D01]/[M01]/[Y0001] [H01]:[m01]:[s01]')"/>
                                    <xsl:value-of select="$fecha2"/>
                                </xsl:if>

                            </endDate>
                            <locations>
                                <xsl:for-each
                                    select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Shipment/*:ShipmentHeader/*:SourceLocationRef/*:LocationRef">
                                    <xsl:variable name="locationRef" select="position()"/>
                                    <location type="">
                                        <id type="" typeName="">
                                            <xsl:attribute name="appCode">
                                                <xsl:value-of
                                                  select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Shipment/*:ShipmentHeader/*:SourceLocationRef/*:LocationRef[$locationRef]/*:LocationGid/*:Gid/*:DomainName"
                                                />
                                            </xsl:attribute>
                                            <xsl:value-of
                                                select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Shipment/*:ShipmentHeader/*:SourceLocationRef/*:LocationRef[$locationRef]/*:LocationGid/*:Gid/*:Xid"
                                            />
                                        </id>
                                        <description/>
                                        <transactionCode/>
                                        <status>
                                            <id/>
                                            <name/>
                                        </status>
                                        <isTemporary/>
                                        <isTemplate/>
                                        <rol>
                                            <id/>
                                            <description/>
                                        </rol>
                                        <address>
                                            <id/>
                                            <street/>
                                            <interiorNumber/>
                                            <exteriorNumber/>
                                            <postalCode/>
                                            <municipality/>
                                            <population/>
                                            <neighborhood/>
                                            <locality/>
                                            <entity/>
                                            <state>
                                                <id/>
                                                <name/>
                                            </state>
                                            <city>
                                                <id/>
                                                <name/>
                                            </city>
                                            <country>
                                                <id/>
                                                <name/>
                                                <code/>
                                            </country>
                                            <timeZone id=""/>
                                            <latitude/>
                                            <allAddress/>
                                            <longitude/>
                                        </address>
                                    </location>
                                </xsl:for-each>
                            </locations>
                            <stops>
                                <stop>
                                    <nameStop/>
                                    <numberStop/>
                                </stop>
                            </stops>
                            <hours/>
                            <transportation>
                                <truck>
                                    <id type="" typeName=""/>
                                    <mileage/>
                                    <enrollment/>
                                    <value/>
                                    <status id="" appCode="">
                                        <id/>
                                        <type/>
                                        <name/>
                                        <value/>
                                    </status>
                                    <equipments>
                                        <equipment>
                                            <id type=""/>
                                            <code/>
                                            <name/>
                                            <cost/>
                                            <status/>
                                        </equipment>
                                    </equipments>
                                </truck>
                            </transportation>
                        </travel>
                        <cedis>
                            <id appCode="" name=""/>
                            <name>
                                <xsl:value-of
                                    select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:PaymentHeader/*:ServiceProviderGid/*:Gid/*:Xid"
                                />
                            </name>
                            <stateCode/>
                        </cedis>
                        <drivers>
                            <driver type="Principal">
                                <id>
                                    <xsl:value-of
                                        select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Shipment/*:ShipmentHeader/*:DriverGid/*:Gid/*:Xid"
                                    />
                                </id>
                                <name/>
                                <status/>
                                <xsl:for-each
                                    select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem">
                                    <xsl:variable name="pos" select="position()"/>
                                    <xsl:variable name="code"
                                        select="replace(//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$pos]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode, '\s', '_')"/>
                                    <xsl:if test="$code = 'MASTER.PAGO_OPERADOR'">

                                        <pay>
                                            <xsl:attribute name="type">
                                                <xsl:value-of
                                                  select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$pos]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode"
                                                />
                                            </xsl:attribute>
                                            <xsl:value-of
                                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$pos]/*:CommonInvoiceLineElements/*:FreightRate/*:FreightCharge/*:FinancialAmount/*:MonetaryAmount"
                                            />
                                        </pay>
                                    </xsl:if>
                                </xsl:for-each>

                                <license>
                                    <folio/>
                                    <expiration/>
                                    <status/>
                                </license>
                                <bankAccount>
                                    <id/>
                                    <name/>
                                    <numberAccount/>
                                    <interbankAccount/>
                                </bankAccount>
                            </driver>
                            <xsl:variable name="nameD"
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Shipment/*:Driver/*:SecondaryDriverGid/*:Gid/*:Xid"/>
                            <xsl:if test="$nameD">
                                <driver type="Secondary">
                                    <id>
                                        <xsl:value-of
                                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Shipment/*:Driver/*:SecondaryDriverGid/*:Gid/*:Xid"
                                        />
                                    </id>
                                    <name/>
                                    <status/>
                                    <xsl:for-each
                                        select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode">
                                        <xsl:variable name="pos" select="position()"/>
                                        <xsl:variable name="code"
                                            select="replace(//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$pos]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode, '\s', '_')"/>
                                        <xsl:if test="$code = 'MASTER.PAGO_MANIOBRISTA'">
                                            <pay>
                                                <xsl:attribute name="type">
                                                  <xsl:value-of
                                                  select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$pos]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode"
                                                  />
                                                </xsl:attribute>
                                                <xsl:value-of
                                                  select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$pos]/*:CommonInvoiceLineElements/*:FreightRate/*:FreightCharge/*:FinancialAmount/*:MonetaryAmount"
                                                />
                                            </pay>
                                        </xsl:if>
                                    </xsl:for-each>
                                    <license>
                                        <folio/>
                                        <expiration/>
                                        <status/>
                                    </license>
                                    <bankAccount>
                                        <id/>
                                        <name/>
                                        <numberAccount/>
                                        <interbankAccount/>
                                    </bankAccount>
                                </driver>
                            </xsl:if>

                        </drivers>
                        <folio/>
                        <lineItems>
                            <xsl:for-each
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem">
                                <lineItem>
                                    <sequence>
                                        <xsl:value-of select="*:AssignedNum"/>
                                    </sequence>
                                    <typeCost>
                                        <xsl:value-of select="*:CostTypeGid/*:Gid/*:Xid"/>
                                    </typeCost>
                                    <accesory>
                                        <id type=""/>
                                        <name/>
                                        <description>
                                            <xsl:value-of
                                                select="*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode"
                                            />
                                        </description>
                                        <code>

                                            <xsl:value-of
                                                select="*:CommonInvoiceLineElements/*:Commodity/*:Description"
                                            />
                                        </code>
                                        <cost>
                                            <xsl:value-of
                                                select="*:CommonInvoiceLineElements/*:FreightRate/*:FreightCharge/*:FinancialAmount/*:MonetaryAmount"
                                            />
                                        </cost>
                                        <generalLedger>
                                            <id/>
                                            <name/>
                                            <code>
                                                <xsl:value-of
                                                  select="*:CommonInvoiceLineElements/*:GeneralLedgerGid/*:Gid/*:Xid"
                                                />
                                            </code>
                                            <description/>
                                        </generalLedger>
                                    </accesory>
                                </lineItem>
                            </xsl:for-each>
                        </lineItems>
                        <currency>
                            <id/>
                            <name/>
                            <code>
                                <xsl:value-of
                                    select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:AmountToPay/*:FinancialAmount/*:GlobalCurrencyCode"
                                />
                            </code>
                            <value/>
                        </currency>
                        <paymentConfig>
                            <payment>
                                <folio/>
                                <amountOfPay>
                                    <xsl:value-of
                                        select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:AmountToPay/*:FinancialAmount/*:MonetaryAmount"
                                    />
                                </amountOfPay>
                                <paymentWay>
                                    <id/>
                                    <name/>
                                </paymentWay>
                                <paymentMethod>
                                    <id/>
                                    <name/>
                                </paymentMethod>
                                <cfdiUse>
                                    <id/>
                                    <name/>
                                </cfdiUse>
                                <paymentDeadline>
                                    <id/>
                                    <name/>
                                    <value/>
                                </paymentDeadline>
                            </payment>

                        </paymentConfig>

                    </xsl:when>
                    <xsl:otherwise>
                        <id>
                            <xsl:attribute name="appCode">
                                <xsl:value-of
                                    select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:VoucherGid/*:Gid/*:DomainName"
                                />
                            </xsl:attribute>
                            <xsl:value-of
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:VoucherGid/*:Gid/*:Xid"
                            />
                        </id>
                        <capturedBy>
                            <xsl:value-of
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:ApprovedBy"
                            />
                        </capturedBy>
                        <approvedDate>
                            <xsl:variable name="aprovate"
                                select="//*:Voucher/*:ApprovedDate/*:GLogDate"/>
                            <xsl:if test="$aprovate">
                                <xsl:variable name="year" select="substring($aprovate, 0, 5)"/>
                                <xsl:variable name="mes" select="substring($aprovate, 5, 2)"/>
                                <xsl:variable name="dia" select="substring($aprovate, 7, 2)"/>
                                <xsl:variable name="hora" select="substring($aprovate, 9, 2)"/>
                                <xsl:variable name="minuto" select="substring($aprovate, 11, 2)"/>
                                <xsl:variable name="seg" select="'00'"/>


                                <xsl:variable name="newDate"
                                    select="concat($year, '-', $mes, '-', $dia)"/>

                                <xsl:variable name="tiemp"
                                    select="concat($hora, ':', $minuto, ':', $seg)"/>

                                <xsl:variable name="timeStamp"
                                    select="dateTime(xs:date($newDate), xs:time($tiemp))"/>
                                <xsl:variable name="fecha2" as="xs:string"
                                    select="format-dateTime($timeStamp, '[D01]/[M01]/[Y0001] [H01]:[m01]:[s01]')"/>
                                <xsl:value-of select="$fecha2"/>
                            </xsl:if>
                        </approvedDate>
                        <creationDate>
                            <xsl:variable name="creationv"
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:ApprovedDate/*:GLogDate"/>
                            <xsl:if test="$creationv">
                                <xsl:variable name="year" select="substring($creationv, 0, 5)"/>
                                <xsl:variable name="mes" select="substring($creationv, 5, 2)"/>
                                <xsl:variable name="dia" select="substring($creationv, 7, 2)"/>
                                <xsl:variable name="hora" select="substring($creationv, 9, 2)"/>
                                <xsl:variable name="minuto" select="substring($creationv, 11, 2)"/>
                                <xsl:variable name="seg" select="'00'"/>


                                <xsl:variable name="newDate"
                                    select="concat($year, '-', $mes, '-', $dia)"/>

                                <xsl:variable name="tiemp"
                                    select="concat($hora, ':', $minuto, ':', $seg)"/>

                                <xsl:variable name="timeStamp"
                                    select="dateTime(xs:date($newDate), xs:time($tiemp))"/>
                                <xsl:variable name="fecha2" as="xs:string"
                                    select="format-dateTime($timeStamp, '[D01]/[M01]/[Y0001] [H01]:[m01]:[s01]')"/>
                                <xsl:value-of select="$fecha2"/>
                            </xsl:if>
                        </creationDate>
                        
                        <consolidationType>
                            <xsl:for-each select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:PaymentHeader/*:ConsolidationType">
                                <xsl:variable name="posd" select="position()"/>
                                <xsl:if test="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:PaymentHeader/*:ConsolidationType[$posd] = 'PARENT'">
                                    <xsl:value-of select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:PaymentHeader/*:ConsolidationType"/>
                                </xsl:if>
                            </xsl:for-each>
                           
                        </consolidationType>
                        <cedis>
                            <id appCode="" name=""/>
                            <name/>
                            <stateCode/>
                        </cedis>
                        <folio/>
                        <currency>
                            <code>
                                <xsl:value-of
                                    select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:PaymentHeader/*:NetAmountDue/*:FinancialAmount/*:GlobalCurrencyCode"
                                />
                            </code>
                        </currency>

                        <approvedBy>
                            <xsl:value-of
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:ApprovedBy"
                            />
                        </approvedBy>
                        <drivers>
                            <driver type="Principal">
                                <id/>
                                <name/>
                                <status/>

                                <pay>
                                    <xsl:attribute name="type"> </xsl:attribute>

                                </pay>


                                <license>
                                    <folio/>
                                    <expiration/>
                                    <status/>
                                </license>
                                <bankAccount>
                                    <id/>
                                    <name/>
                                    <numberAccount/>
                                    <interbankAccount/>
                                </bankAccount>
                            </driver>
                        </drivers>
                        <paymentConfig>
                            <payment>
                                <folio/>
                                <amountOfPay>
                                    <xsl:value-of
                                        select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:PaymentHeader/*:NetAmountDue/*:FinancialAmount/*:MonetaryAmount"
                                    />
                                </amountOfPay>
                                <paymentWay>
                                    <id/>
                                    <name/>
                                </paymentWay>
                                <paymentMethod>
                                    <id/>
                                    <name/>
                                </paymentMethod>
                                <cfdiUse>
                                    <id/>
                                    <name/>
                                </cfdiUse>
                                <paymentDeadline>
                                    <id/>
                                    <name/>
                                    <value/>
                                </paymentDeadline>
                            </payment>
                        </paymentConfig>
                        <childrenVouchers>
                            <xsl:for-each
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:ChildPayments/*:Payment">
                                <xsl:variable name="posV" select="position()"/>
                                <voucher>
                                    <approvedDate>
                                        <xsl:variable name="aprovate"
                                            select="//*:Voucher/*:ApprovedDate/*:GLogDate"/>
                                        <xsl:if test="$aprovate">
                                            <xsl:variable name="year"
                                                select="substring($aprovate, 0, 5)"/>
                                            <xsl:variable name="mes"
                                                select="substring($aprovate, 5, 2)"/>
                                            <xsl:variable name="dia"
                                                select="substring($aprovate, 7, 2)"/>
                                            <xsl:variable name="hora"
                                                select="substring($aprovate, 9, 2)"/>
                                            <xsl:variable name="minuto"
                                                select="substring($aprovate, 11, 2)"/>
                                            <xsl:variable name="seg" select="'00'"/>


                                            <xsl:variable name="newDate"
                                                select="concat($year, '-', $mes, '-', $dia)"/>

                                            <xsl:variable name="tiemp"
                                                select="concat($hora, ':', $minuto, ':', $seg)"/>

                                            <xsl:variable name="timeStamp"
                                                select="dateTime(xs:date($newDate), xs:time($tiemp))"/>
                                            <xsl:variable name="fecha2" as="xs:string"
                                                select="format-dateTime($timeStamp, '[D01]/[M01]/[Y0001] [H01]:[m01]:[s01]')"/>
                                            <xsl:value-of select="$fecha2"/>
                                        </xsl:if>
                                    </approvedDate>
                                    <creationDate>
                                        <xsl:variable name="creationv"
                                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:ApprovedDate/*:GLogDate"/>
                                        <xsl:if test="$creationv">
                                            <xsl:variable name="year"
                                                select="substring($creationv, 0, 5)"/>
                                            <xsl:variable name="mes"
                                                select="substring($creationv, 5, 2)"/>
                                            <xsl:variable name="dia"
                                                select="substring($creationv, 7, 2)"/>
                                            <xsl:variable name="hora"
                                                select="substring($creationv, 9, 2)"/>
                                            <xsl:variable name="minuto"
                                                select="substring($creationv, 11, 2)"/>
                                            <xsl:variable name="seg" select="'00'"/>


                                            <xsl:variable name="newDate"
                                                select="concat($year, '-', $mes, '-', $dia)"/>

                                            <xsl:variable name="tiemp"
                                                select="concat($hora, ':', $minuto, ':', $seg)"/>

                                            <xsl:variable name="timeStamp"
                                                select="dateTime(xs:date($newDate), xs:time($tiemp))"/>
                                            <xsl:variable name="fecha2" as="xs:string"
                                                select="format-dateTime($timeStamp, '[D01]/[M01]/[Y0001] [H01]:[m01]:[s01]')"/>
                                            <xsl:value-of select="$fecha2"/>
                                        </xsl:if>
                                    </creationDate>

                                    <approvedBy>
                                        <xsl:value-of
                                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:ApprovedBy"
                                        />
                                    </approvedBy>
                                    <id>
                                        <xsl:attribute name="appCode">
                                            <xsl:value-of
                                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:VoucherGid/*:Gid/*:DomainName"/>

                                        </xsl:attribute>
                                        <xsl:value-of
                                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:ChildPayments/*:Payment[$posV]/*:PaymentHeader/*:InvoiceGid/*:Gid/*:Xid"
                                        />
                                    </id>
                                    <consolidationType>
                                        <xsl:value-of
                                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:ChildPayments/*:Payment[$posV]/*:PaymentHeader/*:ConsolidationType"
                                        />
                                    </consolidationType>
                                    <currency>
                                        <code>
                                            <xsl:value-of
                                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:PaymentHeader/*:NetAmountDue/*:FinancialAmount/*:GlobalCurrencyCode"
                                            />
                                        </code>
                                    </currency>

                                    <travel>
                                        <id>
                                            <xsl:value-of
                                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Shipment[$posV]/*:ShipmentHeader/*:ShipmentGid/*:Gid/*:Xid"
                                            />
                                        </id>
                                        <endtDate>
                                            <xsl:variable name="dateEn"
                                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Shipment[$posV]/*:ShipmentHeader/*:EndDt/*:GLogDate"/>

                                            <xsl:if test="$dateEn">
                                                <xsl:variable name="year"
                                                  select="substring($dateEn, 0, 5)"/>
                                                <xsl:variable name="mes"
                                                  select="substring($dateEn, 5, 2)"/>
                                                <xsl:variable name="dia"
                                                  select="substring($dateEn, 7, 2)"/>
                                                <xsl:variable name="hora"
                                                  select="substring($dateEn, 9, 2)"/>
                                                <xsl:variable name="minuto"
                                                  select="substring($dateEn, 11, 2)"/>
                                                <xsl:variable name="seg" select="'00'"/>


                                                <xsl:variable name="newDate"
                                                  select="concat($year, '-', $mes, '-', $dia)"/>

                                                <xsl:variable name="tiemp"
                                                  select="concat($hora, ':', $minuto, ':', $seg)"/>

                                                <xsl:variable name="timeStamp"
                                                  select="dateTime(xs:date($newDate), xs:time($tiemp))"/>
                                                <xsl:variable name="fecha2" as="xs:string"
                                                  select="format-dateTime($timeStamp, '[D01]/[M01]/[Y0001] [H01]:[m01]:[s01]')"/>
                                                <xsl:value-of select="$fecha2"/>
                                            </xsl:if>

                                        </endtDate>
                                        <locations>
                                            <xsl:for-each
                                                select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Shipment[$posV]/*:ShipmentHeader/*:SourceLocationRef/*:LocationRef">
                                                <xsl:variable name="locPos" select="position()"/>
                                                <location>
                                                  <id>
                                                  <xsl:attribute name="appCode">
                                                  <xsl:value-of
                                                  select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Shipment[$posV]/*:ShipmentHeader/*:SourceLocationRef/*:LocationRef[$locPos]/*:LocationGid/*:Gid/*:DomainName"
                                                  />
                                                  </xsl:attribute>
                                                  <xsl:value-of
                                                  select="/*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Shipment[$posV]/*:ShipmentHeader/*:SourceLocationRef/*:LocationRef[$locPos]/*:LocationGid/*:Gid/*:Xid"
                                                  />
                                                  </id>
                                                  <description/>
                                                  <transactionCode/>
                                                  <status>
                                                  <id/>
                                                  <name/>
                                                  </status>
                                                  <isTemporary/>
                                                  <isTemplate/>
                                                  <rol>
                                                  <id/>
                                                  <description/>
                                                  </rol>
                                                  <address>
                                                  <id/>
                                                  <street/>
                                                  <interiorNumber/>
                                                  <exteriorNumber/>
                                                  <postalCode/>
                                                  <municipality/>
                                                  <population/>
                                                  <neighborhood/>
                                                  <locality/>
                                                  <entity/>
                                                  <state>
                                                  <id/>
                                                  <name/>
                                                  </state>
                                                  <city>
                                                  <id/>
                                                  <name/>
                                                  </city>
                                                  <country>
                                                  <id/>
                                                  <name/>
                                                  <code/>
                                                  </country>
                                                  <timeZone id=""/>
                                                  <latitude/>
                                                  <allAddress/>
                                                  <longitude/>
                                                  </address>
                                                </location>
                                            </xsl:for-each>
                                        </locations>
                                    </travel>
                                    <drivers>
                                        <driver type="Principal">
                                            <id>
                                                <xsl:value-of
                                                  select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Shipment[$posV]/*:Driver/*:DriverGid/*:Gid/*:Xid"
                                                />
                                            </id>
                                            <xsl:for-each
                                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:ChildPayments/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem">
                                                <xsl:variable name="pos" select="position()"/>
                                                <xsl:variable name="code"
                                                  select="replace(//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:ChildPayments/*:Payment[$posV]/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$pos]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode, '\s', '_')"/>
                                                <xsl:if test="$code = 'MASTER.PAGO_OPERADOR'">

                                                  <pay>
                                                  <xsl:attribute name="type">
                                                  <xsl:value-of
                                                  select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:ChildPayments/*:Payment[$posV]/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$pos]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode"
                                                  />
                                                  </xsl:attribute>
                                                  <xsl:value-of
                                                  select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:ChildPayments/*:Payment[$posV]/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$pos]/*:CommonInvoiceLineElements/*:FreightRate/*:FreightCharge/*:FinancialAmount/*:MonetaryAmount"
                                                  />
                                                  </pay>
                                                </xsl:if>
                                            </xsl:for-each>

                                            <license>
                                                <folio/>
                                                <expiration/>
                                                <status/>
                                            </license>
                                            <bankAccount>
                                                <id/>
                                                <name/>
                                                <numberAccount/>
                                                <interbankAccount/>
                                            </bankAccount>
                                        </driver>
                                        <xsl:variable name="idSecu"
                                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Shipment[$posV]/*:Driver/*:SecondaryDriverGid/*:Gid/*:Xid"/>
                                        <xsl:if test="$idSecu">
                                            <driver type="Secundary">
                                                <id>
                                                  <xsl:value-of
                                                  select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Shipment[$posV]/*:Driver/*:SecondaryDriverGid/*:Gid/*:Xid"
                                                  />
                                                </id>
                                                <xsl:for-each
                                                  select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:ChildPayments/*:Payment[$posV]/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem">
                                                  <xsl:variable name="pos" select="position()"/>
                                                  <xsl:variable name="code"
                                                  select="replace(//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:ChildPayments/*:Payment[$posV]/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$pos]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode, '\s', '_')"/>
                                                  <xsl:if test="$code = 'MASTER.PAGO_MANIOBRISTA'">

                                                  <pay>
                                                  <xsl:attribute name="type">
                                                  <xsl:value-of
                                                  select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:ChildPayments/*:Payment[$posV]/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$pos]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode"
                                                  />
                                                  </xsl:attribute>
                                                  <xsl:value-of
                                                  select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:ChildPayments/*:Payment[$posV]/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$pos]/*:CommonInvoiceLineElements/*:FreightRate/*:FreightCharge/*:FinancialAmount/*:MonetaryAmount"
                                                  />
                                                  </pay>
                                                  </xsl:if>
                                                </xsl:for-each>

                                                <license>
                                                  <folio/>
                                                  <expiration/>
                                                  <status/>
                                                </license>
                                                <bankAccount>
                                                  <id/>
                                                  <name/>
                                                  <numberAccount/>
                                                  <interbankAccount/>
                                                </bankAccount>
                                            </driver>
                                        </xsl:if>
                                    </drivers>

                                    <xsl:for-each
                                        select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment">
                                        <xsl:variable name="payPos" select="position()"/>
                                        <cedis>
                                            <name>
                                                <xsl:value-of
                                                  select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment[$payPos]/*:PaymentHeader/*:ServiceProviderGid/*:Gid/*:Xid"
                                                />
                                            </name>
                                        </cedis>

                                    </xsl:for-each>
                                    <lineItems>
                                        <xsl:for-each
                                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:ChildPayments/*:Payment[$posV]/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem">
                                            <xsl:variable name="seq" select="position()"/>
                                            <lineItem>
                                                <id>
                                                  <xsl:attribute name="type">
                                                  <xsl:value-of
                                                  select="CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode"
                                                  />
                                                  </xsl:attribute>
                                                </id>
                                                <sequence>
                                                  <xsl:value-of
                                                  select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:ChildPayments/*:Payment[$posV]/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$seq]/*:AssignedNum"
                                                  />
                                                </sequence>
                                                <typeCost>
                                                  <xsl:value-of
                                                  select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:ChildPayments/*:Payment[$posV]/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$seq]/*:CostTypeGid/*:Gid/*:Xid"
                                                  />
                                                </typeCost>
                                                <accesory>
                                                  <id type=""/>
                                                  <name/>
                                                  <description>
                                                  <xsl:value-of
                                                  select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:ChildPayments/*:Payment[$posV]/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$seq]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode"
                                                  />
                                                  </description>
                                                  <code>

                                                  <xsl:value-of
                                                  select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:ChildPayments/*:Payment[$posV]/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$seq]/*:CommonInvoiceLineElements/*:Commodity/*:Description"
                                                  />
                                                  </code>
                                                  <cost>
                                                  <xsl:value-of
                                                  select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Voucher/*:Payment/*:ChildPayments/*:Payment[$posV]/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$seq]/*:CommonInvoiceLineElements/*:FreightRate/*:FreightCharge/*:FinancialAmount/*:MonetaryAmount"
                                                  />
                                                  </cost>
                                                  <generalLedger>
                                                  <code>
                                                  <xsl:value-of
                                                  select="*:CommonInvoiceLineElements/*:GeneralLedgerGid/*:Gid/*:Xid"
                                                  />
                                                  </code>
                                                  </generalLedger>
                                                </accesory>
                                            </lineItem>
                                        </xsl:for-each>
                                    </lineItems>


                                </voucher>
                            </xsl:for-each>


                        </childrenVouchers>
                    </xsl:otherwise>
                </xsl:choose>
            </voucherDriver>
        </message>

    </xsl:template>
</xsl:stylesheet>
