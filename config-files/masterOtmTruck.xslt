<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <!--  Status  -->
    <xsl:key name="status" match="lookup/statusInv/string" use="@id"/>
    <xsl:variable name="trucSta" select='document("truckStatus.xml")/lookup/statusInv'/>
    
    <!--  Marca  -->
    <xsl:key name="marca" match="marca/string" use="@id"/>
    <xsl:variable name="marcaT" select='document("marca.xml")/marca'/>
    
    
    <xsl:output indent="yes"/>
    <xsl:template match="/">
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
            xmlns:tran="http://xmlns.oracle.com/apps/otm/TransmissionService"
            xmlns:v6="http://xmlns.oracle.com/apps/otm/transmission/v6.4">
            <soapenv:Header>
                <wsse:Security
                    xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                    <wsse:UsernameToken
                        xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                        <wsse:Username>MASTER.IVAN_ROSALES</wsse:Username>
                        <wsse:Password
                            Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"
                            >Sol140218357</wsse:Password>
                    </wsse:UsernameToken>
                </wsse:Security>
            </soapenv:Header>
            <soapenv:Body>
                <tran:publish>
                    <Transmission xmlns="http://xmlns.oracle.com/apps/otm/transmission/v6.4">
                        <TransmissionHeader/>
                        <TransmissionBody>
                            <GLogXMLElement>
                                <otm:PowerUnit
                                    xmlns:otm="http://xmlns.oracle.com/apps/otm/transmission/v6.4"
                                    xmlns:gtm="http://xmlns.oracle.com/apps/gtm/transmission/v6.4">
                                    <otm:PowerUnitGid>
                                        <otm:Gid>
                                            <otm:DomainName>
                                                <xsl:value-of select="//availableTruck/id/@appCode"
                                                />
                                            </otm:DomainName>
                                            <otm:Xid>
                                                <xsl:value-of select="//availableTruck/truck/id"/>
                                            </otm:Xid>
                                        </otm:Gid>
                                    </otm:PowerUnitGid>
                                    <otm:TransactionCode>IU</otm:TransactionCode>
                                    <otm:PowerUnitHeader>
                                        <otm:PowerUnitNum>
                                            <xsl:value-of select="//availableTruck/truck/plaque"/>
                                        </otm:PowerUnitNum>
                                        <otm:PowerUnitTypeGid>
                                            <xsl:variable name="type" select="//availableTruck/truck/id/@type"/>
                                            <otm:Gid>
                                                <otm:DomainName>MASTER</otm:DomainName>
                                                <otm:Xid>
                                                  <xsl:for-each select="$marcaT">
                                                      <xsl:value-of select="key('marca',$type)"/>
                                                  </xsl:for-each>
                                                </otm:Xid>
                                            </otm:Gid>
                                        </otm:PowerUnitTypeGid>
                                        <otm:IsActive>
                                            <xsl:value-of select="//availableTruck/truck/isActive"/>
                                        </otm:IsActive>
                                        <otm:FlexFieldStrings/>
                                        <otm:FlexFieldNumbers>
                                            <otm:AttributeNumber3>
                                                <xsl:value-of
                                                  select="//availableTruck/truck/mileage"/>
                                            </otm:AttributeNumber3>
                                        </otm:FlexFieldNumbers>
                                        <otm:FlexFieldDates/>
                                    </otm:PowerUnitHeader>
                                    <otm:Status>
                                        <otm:StatusTypeGid>
                                            <otm:Gid>
                                                <otm:DomainName>MASTER</otm:DomainName>
                                                <otm:Xid>TRACTO_STATUS</otm:Xid>
                                            </otm:Gid>
                                        </otm:StatusTypeGid>
                                        <otm:StatusValueGid>
                                            <xsl:variable name="idStatus" select="//availableTruck/truck/status/id"/>
                                            <otm:Gid>
                                                <otm:DomainName>MASTER</otm:DomainName>
                                                <otm:Xid>
                                                    <xsl:for-each select="$trucSta">
                                                        <xsl:value-of select="key('status',$idStatus )"/>
                                                    </xsl:for-each>
                                                </otm:Xid>
                                            </otm:Gid>
                                        </otm:StatusValueGid>
                                    </otm:Status>
                                </otm:PowerUnit>
                            </GLogXMLElement>
                        </TransmissionBody>
                    </Transmission>
                </tran:publish>
            </soapenv:Body>
        </soapenv:Envelope>
    </xsl:template>
</xsl:stylesheet>
