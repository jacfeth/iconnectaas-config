<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs">
    <xsl:output indent="yes"/>
    
    <!--  equipos  -->
    <xsl:key name="equipo" match="equipos/string" use="@id"/>
    <xsl:variable name="equipoE" select='document("equipos.xml")/equipos'/>
    
    <!--  status  -->
    <xsl:key name="estatus" match="status/string" use="@id"/>
    <xsl:variable name="estE" select='document("estatusEquipo.xml")/status'/>
    
    <!-- IsActive -->
    <xsl:key name="active" match="activeEquipo/string" use="@id"/>
    <xsl:variable name="activeE" select='document("activeEquipo.xml")/activeEquipo'/>
    
    <!--  marca  -->
    <xsl:key name="marca" match="marca/string" use="@id"/>
    <xsl:variable name="marcE" select='document("marca.xml")/marca'/>
    
    <!--  proposito  -->
    <xsl:key name="proposito" match="proposito/string" use="@id"/>
    <xsl:variable name="propocitoC" select='document("proposito.xml")/proposito'/>
    
    <!--  owner  -->
    <xsl:key name="onwer" match="owner/string" use="@id"/>
    <xsl:variable name="onwerC" select='document("owner.xml")/owner'/>
    
    <xsl:template match="/">
        <xsl:variable name="date"
            select="string(//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:EquipmentRegistrationDate/*:GLogDate)"/>
        <message>
            <availableEquipment>
                <id appCode="MASTER"/>
                <creationDate>
                    <xsl:if test="$date">
                        <xsl:variable name="year" select="substring($date, 0, 5)"/>
                        <xsl:variable name="mes" select="substring($date, 5, 2)"/>
                        <xsl:variable name="dia" select="substring($date, 7, 2)"/>
                        <xsl:variable name="hora" select="substring($date, 9, 2)"/>
                        <xsl:variable name="minuto" select="substring($date, 11, 2)"/>
                        <xsl:variable name="seg" select="substring($date, 12, 2)"/>
                        
                        <xsl:variable name="newDate" select="concat($year, '-', $mes, '-', $dia)"/>
                        
                        <xsl:variable name="tiemp" select="concat($hora, ':', $minuto, ':', $seg)"/>
                        
                        <xsl:variable name="timeStamp"
                            select="dateTime(xs:date($newDate), xs:time($tiemp))"/>
                        <xsl:variable name="fecha2" as="xs:string"
                            select="format-dateTime($timeStamp, '[D01]/[M01]/[Y0001] [H01]:[m01]:[s01]')"/>
                        <xsl:value-of select="$fecha2"/>
                    </xsl:if>
                    
                    
                </creationDate>
                <equipment>
                    <id>
                        <xsl:attribute name="appCode">
                            <xsl:value-of
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:EquipmentGid/*:Gid/*:DomainName"
                            />
                        </xsl:attribute>
                        <xsl:attribute name="type">
                            <xsl:variable name="brand" select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:EquipmentTypeGid/*:Gid/*:Xid"/>
                            <xsl:value-of select="$brand"/>
                        </xsl:attribute>
                        <xsl:value-of
                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:EquipmentGid/*:Gid/*:Xid"
                        />
                    </id>
                    <code>
                        <xsl:value-of
                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:LicensePlate"
                        />
                    </code>
                    <serialNumber>
                        <xsl:value-of
                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:EquipmentTag1"
                        />
                    </serialNumber>
                    <name/>
                    <iave>
                        <xsl:value-of
                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:EquipmentRegistrationNum"
                        />
                    </iave>
                    <cost/>
                    
                    <engineNumber>
                        <xsl:value-of
                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:EquipmentTag2"
                        />
                    </engineNumber>
                    <brand>
                        <xsl:value-of
                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:EquipmentTag4"
                        />
                    </brand>
                    <model>
                        <xsl:value-of
                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:EquipmentNumber"
                        />
                    </model>
                    
                    <litreCapacity>0</litreCapacity>
                    
                    
                    <carryingCapacity>
                        <xsl:value-of
                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:FlexFieldNumbers/*:AttributeNumber2"
                        />
                    </carryingCapacity>
                    <mileage>
                        <xsl:value-of
                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:FlexFieldNumbers/*:AttributeNumber3"
                        />
                    </mileage>
                    <isActive>
                        <xsl:variable name="isAc" select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:Status[1]/*:StatusValueGid/*:Gid/*:Xid"/>
                       <xsl:for-each select="$activeE">
                           <xsl:value-of select="key('active',$isAc)"/>
                       </xsl:for-each>
                    </isActive>
                    <porpouse>
                        <id>
                            <xsl:variable name="prop" select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:EquipmentTag3"/>
                            <xsl:for-each select="$propocitoC">
                                <xsl:value-of select="key('proposito',$prop)"/>
                            </xsl:for-each>
                        </id>
                        <name>
                            <xsl:value-of
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:EquipmentTag3"
                            />
                        </name>
                        <description/>
                    </porpouse>
                    <dimensions>
                        <depth>
                            <xsl:attribute name="id">MASTER.FONDO</xsl:attribute>
                            <xsl:value-of
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:Refnum[1]/*:RefnumValue"
                            />
                        </depth>
                        <height>
                            <xsl:attribute name="id">MASTER.ALTO</xsl:attribute>
                            <xsl:value-of
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:Refnum[2]/*:RefnumValue"
                            />
                        </height>
                        <width>
                            <xsl:attribute name="id">MASTER.ANCHO</xsl:attribute>
                            <xsl:value-of
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:Refnum[3]/*:RefnumValue"
                            />
                        </width>
                    </dimensions>
                    <doors>
                        <left>N</left>
                        <rigth>N</rigth>
                        <back>N</back>
                        <front>N</front>
                    </doors>
                    <billOfLading>
                        <xsl:choose>
                            <xsl:when test="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:FlexFieldStrings/*:Attribute3">
                                <xsl:value-of
                                    select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:FlexFieldStrings/*:Attribute3"
                                />
                            </xsl:when>
                            <xsl:otherwise>N</xsl:otherwise>
                        </xsl:choose>
                      
                    </billOfLading>
                    <palletsQty>
                        <xsl:value-of
                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:FlexFieldNumbers/*:AttributeNumber6"
                        />
                    </palletsQty>
                    <useAccesory>
                        <xsl:value-of
                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:FlexFieldStrings/*:Attribute1"
                        />
                    </useAccesory>
                    <fuelLoad>
                        <xsl:value-of
                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:FlexFieldStrings/*:Attribute2"
                        />
                    </fuelLoad>
                    <status>
                        <xsl:variable name="name" select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:Status[2]/*:StatusValueGid/*:Gid/*:Xid"/>
                        <id>
                            <xsl:attribute name="appCode">
                                <xsl:value-of
                                    select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:Status[2]/*:StatusValueGid/*:Gid/*:DomainName"
                                />
                            </xsl:attribute>
                            
                        </id>
                        <type>
                            <xsl:value-of
                                select="$name"
                            />
                        </type>
                        <name/>
                        <value>
                            <xsl:for-each select="$estE">
                                <xsl:value-of select="key('estatus',$name)"/>
                            </xsl:for-each>
                        </value>
                    </status>
                    <km>
                        <local>
                            <xsl:value-of
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:FlexFieldNumbers/*:AttributeNumber5"
                            />
                        </local>
                        <foreign>
                            <xsl:value-of
                                select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:FlexFieldNumbers/*:AttributeNumber4"
                            />
                        </foreign>
                    </km>
                </equipment>
                <cedis>
                    <xsl:variable name="on" select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Equipment/*:EquipmentOwner"/>
                    <xsl:variable name="newOn" select="replace($on, '\s','_')"/>
                    <id appCode="MASTER" name="">
                        <xsl:for-each select="$onwerC">
                            <xsl:value-of select="key('onwer',$newOn)"/>
                        </xsl:for-each>
                    </id>
                    <name>
                        <xsl:value-of
                            select="$newOn"/>
                    </name>
                    <stateCode/>
                </cedis>
                
                <comments>
                    <id/>
                    <text/>
                </comments>
            </availableEquipment>
            
        </message>
        
    </xsl:template>
</xsl:stylesheet>
