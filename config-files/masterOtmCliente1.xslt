<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="1.0">
    <xsl:output method="xml" indent="yes"/>
    <!--  Municipio  -->
    <xsl:key name="municipio" match="lookup/municipalyLookup/string" use="@id"/>
    <xsl:variable name="munic" select='document("municipioLookup.xml")/lookup/municipalyLookup'/>
    <!--  poblacion  -->
    <xsl:key name="poblacion" match="lookup/municipalyLookup/string" use="@id"/>
    <xsl:variable name="pobla" select='document("municipioLookup.xml")/lookup/municipalyLookup'/>
    <!--  localidad -->
    <xsl:key name="localidad" match="lookup/municipalyLookup/string" use="@id"/>
    <xsl:variable name="local" select='document("municipioLookup.xml")/lookup/municipalyLookup'/>
    <!-- entidad -->
    <xsl:key name="entidad" match="lookup/municipalyLookup/string" use="@id"/>
    <xsl:variable name="entid" select='document("municipioLookup.xml")/lookup/municipalyLookup'/>
    <!-- provinciaC -->
    <xsl:key name="provinciaC" match="lookup/municipalyLookup/string" use="@id"/>
    <xsl:variable name="provi" select='document("municipioLookup.xml")/lookup/municipalyLookup'/>

    <xsl:template match="/">
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
            xmlns:tran="http://xmlns.oracle.com/apps/otm/TransmissionService"
            xmlns:v6="http://xmlns.oracle.com/apps/otm/transmission/v6.4">
            <soapenv:Header>
                <wsse:Security
                    xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                    <wsse:UsernameToken
                        xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                        <wsse:Username>MASTER.IVAN_ROSALES</wsse:Username>
                        <wsse:Password
                            Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"
                            >Sol140218357</wsse:Password>
                    </wsse:UsernameToken>
                </wsse:Security>
            </soapenv:Header>
            <soapenv:Body>
                <tran:publish>
                    <xsl:variable name="upper" select="customer/clasificationID"/>
                    <xsl:variable name="lower"
                        select="translate($upper, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
                    <xsl:if test="$lower = 'externo'">
                        <xsl:apply-templates select="customer"/>
                    </xsl:if>
                    <xsl:if test="$lower = 'interno'">
                        <xsl:apply-templates select="customer/businessName"/>
                    </xsl:if>
                </tran:publish>
            </soapenv:Body>
        </soapenv:Envelope>
    </xsl:template>
    <xsl:template match="customer">
        <xsl:element xmlns="http://xmlns.oracle.com/apps/otm/transmission/v6.4" name="Transmission">
            <xsl:element name="TransmissionHeader"/>
            <xsl:element name="TransmissionBody">
                <xsl:element name="GLogXMLElement">
                    <xsl:element xmlns:otm="http://xmlns.oracle.com/apps/otm/transmission/v6.4"
                        xmlns:gtm="http://xmlns.oracle.com/apps/gtm/transmission/v6.4"
                        name="otm:Location">
                        <xsl:element name="otm:TransactionCode">IU</xsl:element>
                        <xsl:element name="otm:LocationGid">
                            <xsl:element name="otm:Gid">
                                <xsl:element name="otm:DomainName">MASTER/TRATELOG</xsl:element>
                                <xsl:element name="otm:Xid">
                                    <xsl:value-of select="//customer/locations/location/id"/>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="otm:LocationName">
                            <xsl:value-of select="//customer/fiscalName"/>
                        </xsl:element>
                        <xsl:element name="otm:Description">
                            <xsl:value-of select="//customer/locations/location/description"/>
                        </xsl:element>
                        <xsl:element name="otm:IsTemplate">N</xsl:element>
                        <xsl:element name="otm:Address">
                            <xsl:if test="//customer/locations/location/address/allAddress != ''">
                                <xsl:for-each
                                    select="//customer/locations/location/address/allAddress/allAdress">
                                    <xsl:variable name="pos" select="position()"/>
                                    <xsl:element name="otm:AddressLines">
                                        <xsl:element name="otm:SequenceNumber">
                                            <xsl:value-of select="$pos"/>
                                        </xsl:element>
                                        <xsl:element name="otm:AddressLine">
                                            <xsl:value-of select="."/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:for-each>

                            </xsl:if>
                            <xsl:element name="otm:City">
                                <xsl:variable name="algo2"
                                    select="//customer/locations/location/address/population/id"/>
                                <!--  <xsl:for-each select="$pobla">
                                    <xsl:value-of select="key('poblacion',$algo2 )"/>
                                </xsl:for-each>-->
                                <xsl:value-of
                                    select="//customer/locations/location/address/population/name"/>
                            </xsl:element>
                            <xsl:element name="otm:Province">
                                <xsl:value-of
                                    select="//customer/locations/location/address/state/name"/>
                            </xsl:element>
                            <xsl:element name="otm:ProvinceCode">
                                <xsl:variable name="algo5"
                                    select="//customer/locations/location/address/state/code"/>
                                <!-- <xsl:for-each select="$provi">
                                    <xsl:value-of select="key('provinciaC',$algo5 )"/>
                                </xsl:for-each>-->
                                <xsl:value-of select="$algo5"/>
                            </xsl:element>
                            <xsl:element name="otm:PostalCode">
                                <xsl:value-of
                                    select="//customer/locations/location/address/postalCode"/>
                            </xsl:element>
                            <xsl:element name="otm:CountryCode3Gid">
                                <xsl:element name="otm:Gid">
                                    <xsl:element name="otm:Xid">MX</xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="otm:CountryCode">
                                <xsl:element name="otm:CountryCode3Gid">
                                    <xsl:element name="otm:Gid">
                                        <xsl:element name="otm:Xid">MX</xsl:element>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="otm:TimeZoneGid">
                                <xsl:element name="otm:Gid">
                                    <xsl:element name="otm:Xid">Mexico/General</xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:if test="//customer/locations/location/address/latitude != ''">
                                <xsl:element name="otm:Latitude">
                                    <xsl:value-of
                                        select="//customer/locations/location/address/latitude"/>
                                </xsl:element>
                            </xsl:if>
                            <xsl:if test="//customer/locations/location/address/length != ''">
                                <xsl:element name="otm:Longitude">
                                    <xsl:value-of
                                        select="//customer/locations/location/address/length"/>
                                </xsl:element>
                            </xsl:if>
                        </xsl:element>
                        <xsl:element name="otm:IsTemporary">N</xsl:element>
                        <xsl:element name="otm:LocationRole">
                            <xsl:element name="otm:LocationRoleGid">
                                <xsl:element name="otm:Gid">
                                    <xsl:element name="otm:Xid">BILL TO</xsl:element>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                        <xsl:if test="//customer/description != ''">
                            <xsl:element name="otm:Description">
                                <xsl:value-of select="//customer/locations/location/description"/>
                            </xsl:element>
                        </xsl:if>
                        <xsl:element name="otm:IsActive">
                            <xsl:variable name="data"
                                select="//customer/locations/location/status/name"/>

                            <xsl:variable name="lower"
                                select="translate($data, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
                            <xsl:if test="$lower = 'active'">Y</xsl:if>
                            <xsl:if test="$lower != 'active'">N</xsl:if>
                        </xsl:element>


                        <xsl:element name="otm:FlexFieldStrings">
                            <xsl:element name="otm:Attribute1">
                                <xsl:value-of select="//customer/clasificationID"/>
                            </xsl:element>
                            <xsl:element name="otm:Attribute2">
                                <xsl:variable name="algo"
                                    select="//customer/locations/location/address/municipality/name"/>
                                <!-- <xsl:for-each select="$munic">
                                    <xsl:value-of select="key('municipio',$algo )"/>
                                </xsl:for-each>-->
                                <xsl:value-of select="$algo"/>
                            </xsl:element>
                            <xsl:element name="otm:Attribute4">
                                <xsl:variable name="algo3"
                                    select="//customer/locations/location/address/locality/id"/>
                                <!-- <xsl:for-each select="$local">
                                    <xsl:value-of select="key('localidad',$algo3 )"/>
                                </xsl:for-each>-->

                                <xsl:value-of select="$algo3"/>
                            </xsl:element>
                            <xsl:element name="otm:Attribute5">
                                <xsl:value-of
                                    select="//customer/locations/location/address/neighborhood"/>
                            </xsl:element>
                            <xsl:element name="otm:Attribute6">
                                <xsl:value-of
                                    select="//customer/locations/location/address/externalNumber"/>
                            </xsl:element>
                            <xsl:element name="otm:Attribute7">
                                <xsl:value-of
                                    select="//customer/locations/location/address/interiorNumber"/>
                            </xsl:element>
                            <xsl:element name="otm:Attribute8">
                                <xsl:variable name="algo4"
                                    select="//customer/locations/location/address/entity"/>
                                <!--<xsl:for-each select="$entid">
                                    <xsl:value-of select="key('entidad',$algo4 )"/>
                                </xsl:for-each>-->
                                <xsl:value-of select="$algo4"/>
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="otm:Corporation">
                            <xsl:element name="otm:CorporationGid">
                                <xsl:element name="otm:Gid">
                                    <xsl:element name="otm:DomainName">MASTER/TRATELOG</xsl:element>
                                    <xsl:element name="otm:Xid">
                                        <xsl:value-of select="//customer/rfc"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="otm:TransactionCode">IU</xsl:element>
                            <xsl:element name="otm:CorporationName">
                                <xsl:value-of select="//customer/fiscalName"/>
                            </xsl:element>
                            <xsl:element name="otm:VatProvincialRegistration">
                                <xsl:element name="otm:VatProvincialRegGid">
                                    <xsl:element name="otm:Gid">
                                        <xsl:element name="otm:DomainName"
                                            >MASTER/TRATELOG</xsl:element>
                                        <xsl:element name="otm:Xid">
                                            <xsl:value-of select="//customer/thirdCustomerType"/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="otm:CountryCode3Gid">
                                    <xsl:element name="otm:Gid">
                                        <xsl:element name="otm:Xid">MX</xsl:element>
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="otm:ProvinceCode">
                                    <xsl:value-of
                                        select="//customer/locations/location/address/state/code"/>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="otm:Contact">
                            <xsl:element name="otm:ContactGid">
                                <xsl:element name="otm:Gid">
                                    <xsl:element name="otm:DomainName">MASTER/TRATELOG</xsl:element>
                                    <xsl:element name="otm:Xid">
                                        <xsl:value-of
                                            select="//customer/locations/location/contacts/contact/id"
                                        />
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="otm:TransactionCode">IU</xsl:element>
                            <xsl:element name="otm:EmailAddress">
                                <xsl:value-of
                                    select="//customer/locations/location/contacts/contact/mail"/>
                            </xsl:element>
                            <xsl:element name="otm:FirstName">
                                <xsl:value-of
                                    select="//customer/locations/location/contacts/contact/name"/>
                            </xsl:element>
                            <xsl:element name="otm:Phone1">
                                <xsl:value-of
                                    select="//customer/locations/location/contacts/contact/phone"/>
                            </xsl:element>
                            <xsl:element name="otm:CommunicationMethod">
                                <xsl:element name="otm:ComMethodRank">1</xsl:element>
                                <xsl:element name="otm:ComMethodGid">
                                    <xsl:element name="otm:Gid">
                                        <xsl:element name="otm:Xid">EMAIL</xsl:element>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="otm:IsPrimaryContact">
                                <xsl:choose>
                                    <xsl:when test="//customer/contacts/contact/firstContact != 'Y'">N</xsl:when>
                                    <xsl:otherwise>Y</xsl:otherwise>
                                </xsl:choose>
                            </xsl:element>
                            <xsl:element name="otm:LocationGid">
                                <xsl:element name="otm:Gid">
                                    <xsl:element name="otm:DomainName">MASTER/TRATELOG</xsl:element>
                                    <xsl:element name="otm:Xid">
                                        <xsl:value-of select="//customer/locations/location/id"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="otm:IsNotificationOn">Y</xsl:element>
                            <xsl:element name="otm:Status">
                                <xsl:element name="otm:StatusTypeGid">
                                    <xsl:element name="otm:Gid">
                                        <xsl:element name="otm:DomainName"
                                            >MASTER/TRATELOG</xsl:element>
                                        <xsl:element name="otm:Xid">RPLS</xsl:element>
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="otm:StatusValueGid">
                                    <xsl:element name="otm:Gid">
                                        <xsl:element name="otm:DomainName"
                                            >MASTER/TRATELOG</xsl:element>
                                        <xsl:element name="otm:Xid">RPLS_NOT STARTED</xsl:element>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    <xsl:template match="customer/businessName">
        <xsl:element xmlns="http://xmlns.oracle.com/apps/otm/transmission/v6.4" name="Transmission">
            <xsl:element name="TransmissionHeader"/>
            <xsl:element name="TransmissionBody">
                <xsl:element name="GLogXMLElement">

                    <xsl:element xmlns:otm="http://xmlns.oracle.com/apps/otm/transmission/v6.4"
                        xmlns:gtm="http://xmlns.oracle.com/apps/gtm/transmission/v6.4"
                        name="otm:Location">
                        <xsl:element name="otm:TransactionCode">IU</xsl:element>
                        <xsl:element name="otm:LocationGid">
                            <xsl:element name="otm:Gid">
                                <xsl:element name="otm:DomainName">MASTER</xsl:element>
                                <xsl:element name="otm:Xid">
                                    <xsl:value-of select="//customer/locations/location/id"/>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="otm:LocationName">
                            <xsl:value-of select="//customer/fiscalName"/>
                        </xsl:element>
                        <xsl:element name="otm:Description">
                            <xsl:value-of select="//customer/locations/location/description"/>
                        </xsl:element>
                        <xsl:element name="otm:IsTemplate">N</xsl:element>
                        <xsl:element name="otm:Address">
                            <xsl:if test="//customer/locations/location/address/allAddress != ''">
                                <xsl:for-each
                                    select="//customer/locations/location/address/allAddress/allAdress">
                                    <xsl:variable name="pos" select="position()"/>
                                    <xsl:element name="otm:AddressLines">
                                        <xsl:element name="otm:SequenceNumber">
                                            <xsl:value-of select="$pos"/>
                                        </xsl:element>
                                        <xsl:element name="otm:AddressLine">
                                            <xsl:value-of select="."/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:for-each>

                            </xsl:if>
                            <xsl:element name="otm:City">
                                <xsl:variable name="algo2"
                                    select="//customer/locations/location/address/population/id"/>
                                <!--  <xsl:for-each select="$pobla">
                                    <xsl:value-of select="key('poblacion',$algo2 )"/>
                                </xsl:for-each>-->
                                <xsl:value-of
                                    select="//customer/locations/location/address/population/name"/>
                            </xsl:element>
                            <xsl:element name="otm:Province">
                                <xsl:value-of
                                    select="//customer/locations/location/address/state/name"/>
                            </xsl:element>
                            <xsl:element name="otm:ProvinceCode">
                                <xsl:variable name="algo5"
                                    select="//customer/locations/location/address/state/code"/>
                                <!-- <xsl:for-each select="$provi">
                                    <xsl:value-of select="key('provinciaC',$algo5 )"/>
                                </xsl:for-each>-->
                                <xsl:value-of select="$algo5"/>
                            </xsl:element>
                            <xsl:element name="otm:PostalCode">
                                <xsl:value-of
                                    select="//customer/locations/location/address/postalCode"/>
                            </xsl:element>
                            <xsl:element name="otm:CountryCode3Gid">
                                <xsl:element name="otm:Gid">
                                    <xsl:element name="otm:Xid">MX</xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="otm:CountryCode">
                                <xsl:element name="otm:CountryCode3Gid">
                                    <xsl:element name="otm:Gid">
                                        <xsl:element name="otm:Xid">MX</xsl:element>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="otm:TimeZoneGid">
                                <xsl:element name="otm:Gid">
                                    <xsl:element name="otm:Xid">Mexico/General</xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:if test="//customer/locations/location/address/latitude != ''">
                                <xsl:element name="otm:Latitude">
                                    <xsl:value-of
                                        select="//customer/locations/location/address/latitude"/>
                                </xsl:element>
                            </xsl:if>
                            <xsl:if test="//customer/locations/location/address/length != ''">
                                <xsl:element name="otm:Longitude">
                                    <xsl:value-of
                                        select="//customer/locations/location/address/length"/>
                                </xsl:element>
                            </xsl:if>
                        </xsl:element>
                        <xsl:element name="otm:IsTemporary">N</xsl:element>
                        <xsl:element name="otm:LocationRole">
                            <xsl:element name="otm:LocationRoleGid">
                                <xsl:element name="otm:Gid">
                                    <xsl:element name="otm:Xid">SHIPFROM/SHIPTO</xsl:element>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                        <xsl:if test="//customer/description != ''">
                            <xsl:element name="otm:Description">
                                <xsl:value-of select="//customer/locations/location/description"/>
                            </xsl:element>
                        </xsl:if>
                        <xsl:element name="otm:IsActive">
                            <xsl:variable name="data"
                                select="//customer/locations/location/status/name"/>

                            <xsl:variable name="lower"
                                select="translate($data, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
                            <xsl:if test="$lower = 'active'">Y</xsl:if>
                            <xsl:if test="$lower != 'active'">N</xsl:if>
                        </xsl:element>


                        <xsl:element name="otm:FlexFieldStrings">
                            <xsl:element name="otm:Attribute1">
                                <xsl:value-of select="//customer/clasificationID"/>
                            </xsl:element>
                            <xsl:element name="otm:Attribute2">
                                <xsl:variable name="algo"
                                    select="//customer/locations/location/address/municipality/name"/>
                                <!-- <xsl:for-each select="$munic">
                                    <xsl:value-of select="key('municipio',$algo )"/>
                                </xsl:for-each>-->
                                <xsl:value-of select="$algo"/>
                            </xsl:element>
                            <xsl:element name="otm:Attribute4">
                                <xsl:variable name="algo3"
                                    select="//customer/locations/location/address/locality/id"/>
                                <!-- <xsl:for-each select="$local">
                                    <xsl:value-of select="key('localidad',$algo3 )"/>
                                </xsl:for-each>-->

                                <xsl:value-of select="$algo3"/>
                            </xsl:element>
                            <xsl:element name="otm:Attribute5">
                                <xsl:value-of
                                    select="//customer/locations/location/address/neighborhood"/>
                            </xsl:element>
                            <xsl:element name="otm:Attribute6">
                                <xsl:value-of
                                    select="//customer/locations/location/address/externalNumber"/>
                            </xsl:element>
                            <xsl:element name="otm:Attribute7">
                                <xsl:value-of
                                    select="//customer/locations/location/address/interiorNumber"/>
                            </xsl:element>
                            <xsl:element name="otm:Attribute8">
                                <xsl:variable name="algo4"
                                    select="//customer/locations/location/address/entity"/>
                                <!--<xsl:for-each select="$entid">
                                    <xsl:value-of select="key('entidad',$algo4 )"/>
                                </xsl:for-each>-->
                                <xsl:value-of select="$algo4"/>
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="otm:Corporation">
                            <xsl:element name="otm:CorporationGid">
                                <xsl:element name="otm:Gid">
                                    <xsl:element name="otm:DomainName">MASTER</xsl:element>
                                    <xsl:element name="otm:Xid">FGU830930</xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="otm:TransactionCode">IU</xsl:element>
                            <xsl:element name="otm:CorporationName">
                                <xsl:value-of select="//customer/fiscalName"/>
                            </xsl:element>
                            <xsl:element name="otm:VatProvincialRegistration">
                                <xsl:element name="otm:VatProvincialRegGid">
                                    <xsl:element name="otm:Gid">
                                        <xsl:element name="otm:DomainName">MASTER</xsl:element>
                                        <xsl:element name="otm:Xid">
                                            <xsl:value-of select="//customer/thirdCustomerType"/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="otm:CountryCode3Gid">
                                    <xsl:element name="otm:Gid">
                                        <xsl:element name="otm:Xid">MX</xsl:element>
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="otm:ProvinceCode">
                                    <xsl:value-of
                                        select="//customer/locations/location/address/state/code"/>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="otm:Contact">
                            <xsl:element name="otm:ContactGid">
                                <xsl:element name="otm:Gid">
                                    <xsl:element name="otm:DomainName">MASTER</xsl:element>
                                    <xsl:element name="otm:Xid">
                                        <xsl:value-of
                                            select="//customer/locations/location/contacts/contact/name"
                                        />
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="otm:TransactionCode">IU</xsl:element>
                            <xsl:element name="otm:EmailAddress">
                                <xsl:value-of
                                    select="//customer/locations/location/contacts/contact/mail"/>
                            </xsl:element>
                            <xsl:element name="otm:FirtsName">
                                <xsl:value-of
                                    select="//customer/locations/location/contacts/contact/id"/>
                            </xsl:element>
                            <xsl:element name="otm:Phone1">
                                <xsl:value-of
                                    select="//customer/locations/location/contacts/contact/phone"/>
                            </xsl:element>
                            <xsl:element name="otm:CommunicationMethod">
                                <xsl:element name="otm:ComMethodRank">1</xsl:element>
                                <xsl:element name="otm:ComMethodGid">
                                    <xsl:element name="otm:Gid">
                                        <xsl:element name="otm:Xid">EMAIL</xsl:element>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="otm:IsPrimaryContact">Y</xsl:element>
                            <xsl:element name="otm:LocationGid">
                                <xsl:element name="otm:Gid">
                                    <xsl:element name="otm:DomainName">MASTER</xsl:element>
                                    <xsl:element name="otm:Xid">
                                        <xsl:value-of select="//customer/locations/location/id"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="otm:IsNotificationOn">Y</xsl:element>
                            <xsl:element name="otm:Status">
                                <xsl:element name="otm:StatusTypeGid">
                                    <xsl:element name="otm:Gid">
                                        <xsl:element name="otm:DomainName">MASTER</xsl:element>
                                        <xsl:element name="otm:Xid">RPLS</xsl:element>
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="otm:StatusValueGid">
                                    <xsl:element name="otm:Gid">
                                        <xsl:element name="otm:DomainName">MASTER</xsl:element>
                                        <xsl:element name="otm:Xid">RPLS_NOT STARTED</xsl:element>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
